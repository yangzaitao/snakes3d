import { _decorator, Component, director, instantiate, Node, Prefab, resources } from 'cc';
import { SCENES } from '../../script/config/Scene.enum';
import { Leve } from '../../script/scenes/Leve';
const { ccclass, type } = _decorator;

@ccclass('Venue')
export class Venue extends Component {
    @type(Node)
    StopUI: Node;
    @type(Node)
    MapNode: Node;
    @type(Node)
    Npc: Node;

    protected start(): void {
        this.setLeve();
    }

    /**显示隐藏ui */
    showUI(ent: Event, key: 'stop' | 'continue') {
        this.StopUI.active = !this.StopUI.active; //显示隐藏ui
        if (key === 'stop') director.pause();   //场景暂停
        else director.resume();
    }

    /**退出当前场景 */
    exit() {
        director.resume();
        director.loadScene(SCENES.INDEX);
    }

    /**设置关卡 */
    setLeve() {
        //重置位置
        const post = this.Npc.position;
        this.Npc.setPosition(post.x, 1, post.z);
        this.Npc.active = false;
        //加载关卡预制体并添加到当前节点下
        resources.load(String('leve/').concat(Leve.leve), Prefab, (err, leve_prefab) => {
            if (err === undefined) {
                const leveNode = instantiate(leve_prefab);
                this.MapNode.children.length = 1;
                this.MapNode.addChild(leveNode);
                this.Npc.active = true;
            }
        });
    }
}


