import { _decorator, Component, math, Node, SkeletalAnimation, Vec3 } from 'cc';
import { BtnKEY, ControlBtn } from './ControlBtn';
const { ccclass, type } = _decorator;

@ccclass('node')
export class node extends Component {
    @type(SkeletalAnimation)
    skelet: SkeletalAnimation;
    speed = 2;

    start() {
        ControlBtn.onSETKEY = (key) => {
            switch (key) {
                case BtnKEY.UP: this.node.setRotationFromEuler(new Vec3(0, 180, 0)); break;
                case BtnKEY.DOWN: this.node.setRotationFromEuler(new Vec3(0, 0, 0)); break;
                case BtnKEY.LEFT: this.node.setRotationFromEuler(new Vec3(0, -90, 0)); break;
                case BtnKEY.RIGHT: this.node.setRotationFromEuler(new Vec3(0, 90, 0)); break;
            }
        }
    }

    update(deltaTime: number) {
        if (ControlBtn.KEY != BtnKEY.NULL) {
            const post = this.node.getPosition();
            switch (ControlBtn.KEY) {
                case BtnKEY.UP: post.z -= this.speed * deltaTime; break;
                case BtnKEY.DOWN: post.z += this.speed * deltaTime; break;
                case BtnKEY.LEFT: post.x -= this.speed * deltaTime; break;
                case BtnKEY.RIGHT: post.x += this.speed * deltaTime; break;
            }
            this.node.setPosition(post);
        }
    }
}


