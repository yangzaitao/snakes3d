import { _decorator, Component, Label, Node, NodeEventType } from 'cc';
const { ccclass, type } = _decorator;

export enum BtnKEY {
    UP = "UP",
    DOWN = "DOWN",
    LEFT = "LEFT",
    RIGHT = "RIGHT",
    NULL = "",
}

@ccclass('ControlBtn')
export class ControlBtn extends Component {
    static KEY: BtnKEY = BtnKEY.NULL;
    static onSETKEY: (key: BtnKEY) => void;
    @type([Label])
    btns: Label[] = [];
    start() {
        for (let i = 0; i < this.btns.length; i++) {
            const btn = this.btns[i].node;
            btn.on(NodeEventType.MOUSE_DOWN, this.ondown);
            btn.on(NodeEventType.TOUCH_START, this.ondown);
        }
    }
    ondown(ent: { target: Node }) {
        const node = ent.target;
        ControlBtn.KEY = node.name as BtnKEY;
        ControlBtn.onSETKEY(ControlBtn.KEY);  //按下的时候就调用
        //抬起去掉key值(重置)
        node.once(NodeEventType.MOUSE_UP, reset);
        node.once(NodeEventType.MOUSE_LEAVE, reset);
        node.once(NodeEventType.TOUCH_END, reset);
        node.once(NodeEventType.TOUCH_CANCEL, reset);
        function reset() {
            ControlBtn.KEY = BtnKEY.NULL;
        }
    }
}