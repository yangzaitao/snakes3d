import { _decorator, Component, director, Prefab, Node, instantiate, Label, Touch, Input } from 'cc';
import { SCENES } from '../config/Scene.enum';
import { GAMELEVE, LeveKey } from '../config/game.config';
import { Loading } from './Loding';
const { ccclass, type } = _decorator;
/**此类挂在leve.scene场景里使用 */
@ccclass('Leve')
export class Leve extends Component {
    /**关卡盒子Node节点 */
    @type(Node)
    Gird: Node;
    /**关卡按钮Item预制体 */
    @type(Prefab)
    Item: Prefab;
    /**用来保存当前第几关 */
    static leve: LeveKey = GAMELEVE[0];
    static leveObj = GAMELEVE;//格式化成{a:obj}这种格式

    /**打开index.scene场景（返回首页）*/
    toIndex() {
        director.loadScene(SCENES.INDEX);
    }
    protected start(): void {
        for (let i = 0; i < Leve.leveObj.length; i++) {
            this.addItem(i, Leve.leveObj[i]);
        }
        console.log(Leve.nextLeve());
        console.log(Leve.nextLeve());

    }
    /**添加一个Item子元素（关卡按钮） */
    addItem(num: number, name: LeveKey) {
        const item = instantiate(this.Item); //实例化预制体变成节点
        const label = item.children[0].getComponent(Label);//在item的这个节点里面找到子节点再拿到Label节点组件
        label.string = String(num + 1);//设置文字内容
        item.once(Input.EventType.TOUCH_START, function () {
            Loading.loding()
            //全局保存当前点了第几关
            Leve.leve = name;
        });
        this.Gird.addChild(item);//添加到Gird下面
    }
    /**当前关卡key换成下一关 */
    static nextLeve(): boolean {
        let leve = this.leveObj.indexOf(this.leve);
        this.leve = this.leveObj[leve + 1];
        if (this.leve !== undefined) return true;
        else return false;
    }
}


