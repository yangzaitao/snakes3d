import { _decorator, Component, director} from 'cc';
import { SCENES } from '../config/Scene.enum';
import { Loading } from './Loding';
const { ccclass } = _decorator;
/**此类挂在index.scene场景里使用 */
@ccclass('index')
export class index extends Component {
    onStart() {
        Loading.loding();
    }
    onLeve() {
        director.loadScene(SCENES.LEVE);
    }
}


