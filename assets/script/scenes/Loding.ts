import { _decorator, Component, director, ProgressBar, resources } from 'cc';
import { SCENES } from '../config/Scene.enum';
const { ccclass, type } = _decorator;
/**此类挂在loding.scene场景里使用 */
@ccclass('Loading')
export class Loading extends Component {

    /**场景的进度条节点 */
    @type(ProgressBar)
    probar: ProgressBar;

    /**标识用来判断打开那个场景的 */
    static scene: SCENES;

    /**如果只是调用没有传参数就默认赋值SCENES.VENUE */
    static loding(scene: SCENES = SCENES.VENUE) {
        this.scene = scene;
        //加载Loding场景继续以下资源加载进度条动画
        director.loadScene(SCENES.LODING);
    }

    protected start() {
        //如果scene已经被赋值了
        if (Loading.scene !== undefined) {
            //resources预加载场景名称为Loading.scene的资源（主要用于更新进度条动画）
            resources.preloadScene(Loading.scene, (fin: number, total: number) => {
                //更新进度条动画
                this.probar.progress = (fin + total) * 0.01;
            }, (err) => {
                //如果加载名称为Loading.scene场景的资源失败，就跳出21-24这个代码块不往下执行
                if (err !== undefined) return;
                //加载场景名称为Loading.scene的场景完成就切换
                director.loadScene(Loading.scene);
            })
        }
    }
}